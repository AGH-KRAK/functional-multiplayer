#!/bin/bash

echo "Building Haskell stuff..."
cd hslogic
./setup_proto.sh
cabal build --builddir=../build/hs/

echo "Building Python stuff..."
cd ../pyengine
./setup_proto.sh

echo "Linking binaries to /build"
cd ../build
ln -fs hs/build/server/server .
ln -fs hs/build/pseudoserver/pseudoserver .
ln -fs ../pyengine/build/client .
ln -fs ../pyengine/build/pseudo_client .

echo "All done!"
