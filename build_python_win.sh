#!/bin/bash

PYTHON_BUILD_WORKAROUND=1
echo "Building Python stuff..."
cd ./pyengine
if [ ! -f venv/Scripts/activate ]; then
  virtualenv venv
  source venv/Scripts/activate
  pip install -r requirements.txt
fi
source venv/Scripts/activate
if [ $PYTHON_BUILD_WORKAROUND -eq 0 ] ; then
  python setup.py build_apps --build-base=../build/py/
fi
./setup_proto.sh

echo "Moving binaries to /build"
cd ..
if [ ! -d build ]; then
  mkdir build
fi
cd build
# Work around not working python build
if [ $PYTHON_BUILD_WORKAROUND -eq 0 ] ; then
  cp py/manylinux1_x86_64/client .
  cp py/manylinux1_x86_64/panda_instance .
else
  cp ../pyengine/venv/Scripts/activate .
  cp ../pyengine/build/client.bat .
  cp ../pyengine/build/pseudo_client.bat .
fi

echo "All done!"
read -rsp $'Press enter to continue...\n'
