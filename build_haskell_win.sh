#!/bin/bash

echo "Building Haskell stuff..."
cd hslogic
stack build --only-dependencies --no-docker
./setup_proto_stack.sh
stack build --no-docker
STACK_OUTPUT_DIR=`stack  --no-docker --stack-yaml stack.yaml path --local-install-root`


echo "Moving binaries to /build"
cd ..
if [ ! -d build ]; then
  mkdir build
fi
cd build
cp "${STACK_OUTPUT_DIR}/bin/server" .
cp "${STACK_OUTPUT_DIR}/bin/pseudoserver" .

echo "All done!"
read -rsp $'Press enter to continue...\n'
