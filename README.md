# Functional Multiplayer

A multiplayer gameplay engine built on panda3d and a functional server.

## Requirements
**Git**

We use submodules. Set `git config --global submodule.recurse true`

On first use, run `git submodule update --init --recursive`

**Python**

Install `pip3` and `python3.7` from your package manager

When first working on the project, run

`pip3 install -r requirements.txt`

**Haskell**

Install `haskell-stack`. Anything else will be loaded through stack.

## Building
run build.sh on linux, or write a .bat file to do the same thing on windows.

*If you have ghc installed and don't mind a bit of clutter, you can use  `cabal install` instead of stack (or `cabal install --bindir ./build`).*

*Cross-compiling haskell components between systems is possible if we want to compile our own ghc, but for now windows machines should build for windows and linux machines for linux.*

