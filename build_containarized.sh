#!/bin/bash

PYTHON_BUILD_WORKAROUND=1

echo "Building Haskell stuff..."
cd hslogic
stack docker pull
stack build --only-dependencies
./setup_proto_stack.sh
stack build
STACK_OUTPUT_DIR=`stack --stack-yaml stack.yaml path --local-install-root`

echo "Building Python stuff..."
cd ../pyengine
if [ ! -f venv/bin/activate ]; then
  virtualenv venv
  source venv/bin/activate
  pip3 install -r requirements.txt
fi
source venv/bin/activate
if [ $PYTHON_BUILD_WORKAROUND -eq 0 ] ; then
  python3 setup.py build_apps --build-base=../build/py/
fi
./setup_proto.sh

echo "Moving binaries to /build"
cd ..
if [ ! -d build ]; then
  mkdir build
fi
cd build
ln -fs "${STACK_OUTPUT_DIR}/bin/server" .
ln -fs "${STACK_OUTPUT_DIR}/bin/pseudoserver" .
# Work around not working python build
if [ $PYTHON_BUILD_WORKAROUND -eq 0 ] ; then
  ln -fs py/manylinux1_x86_64/client .
  ln -fs py/manylinux1_x86_64/panda_instance .
else
  ln -fs ../pyengine/venv/bin/activate .
  ln -fs ../pyengine/build/client .
  ln -fs ../pyengine/build/pseudo_client .
fi

echo "All done!"
